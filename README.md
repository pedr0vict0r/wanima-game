# Wanima Game

O Wanima Game é um jogo educativo com propósito de ensinar os sons de animais, seus nomes, e sua pronúncia correta, ensinando de uma forma interativa e divertida.

Consiste em um deck de cartas e um box interativo que em conjunto com um aplicativo para dispositivos móveis formam um jogo de reconhecimento de sons e nomes de animais.


## Especificações Técnicas

### Software e Hardware

#### Aplicativo Móvel

* Plataforma Android © (Versão 4.4+ ou superior)
* Linguagem de programação Java

#### Box Interativo

* Arduino © (plataforma de prototipagem eletrônica de hardware)
* Modelo: Arduino Nano
* Microcontrolador Atmel AVR (ATmega328)
* Linguagem de programação padrão (baseada em C/C++)
* Módulo Bluetooth RS232 HC-05
* Circuito Amplificador de Sinal com CI LM380/386
* Auto-falante de 8 Ohm.

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Construído com

* [Arduino](https://www.arduino.cc/) - Plataforma de hardware e software open-source.
* [Android Studio](https://developer.android.com/studio/index.html) - IDE para a plataforma Android.

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc

